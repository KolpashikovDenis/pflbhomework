import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Main {
    static List<String> lines;
    final static String FILE_NAME = "main.log.2014-11-17";
    static int parts = 0;
    static int count_lines = 0;
    static int last_count_lines = 0;

    public static void main(String[] args) {
        if(args.length >= 2) {
            try {
                Random r = new Random();
                parts = r.nextInt(6) + 5;
                lines = Files.readAllLines(Paths.get(args[0]), StandardCharsets.ISO_8859_1);

                count_lines = lines.size() / parts;

                // Создаем папку в которой создадим наши файлы
                Date curDate = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
                File myDir = new File(sdf.format(new Date()));
                if (!myDir.exists()) {
                    myDir.mkdir();
                }

                String part_of_name = args[1];
                int index = 0;
                int n = 0;
                for (int i = 0; i < parts - 1; i++) {
                    // TODO: создание и запись в файл, постоянная часть имени файла задана в параметрах
                    String name = String.format("%s_%d", part_of_name, i);
                    File file = new File(myDir+"\\"+name+".log");
                    file.createNewFile();
                    file.setWritable(true);
                    OutputStream fOut = new FileOutputStream(file);
                    OutputStreamWriter fWriter = new OutputStreamWriter(fOut, "ISO-8859-1");

                    for(n = index; n < index+count_lines; n++){
                        fWriter.write(lines.get(n)+"\r\n");
                    }
                    fWriter.flush();
                    index += count_lines;
                }

                // Запись последнего куска файла
                String name = String.format("%s_%d", part_of_name, parts-1);
                File file = new File(myDir+"\\"+name+".log");
                file.createNewFile();
                file.setWritable(true);
                OutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter fWriter = new OutputStreamWriter(fOut);

                for(n = index; n < lines.size(); n++){
                    fWriter.write(lines.get(n)+"\r\n");
                }
                fWriter.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Done.");
        } else {
            System.out.println("Not enough arguments.");
        }
    }
}
